/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_1;

/**
 *
 * @author asus
 */
public class Main {
     
    public static void main(String[] args) {

        //obyek UKM
        UKM ukm = new UKM("Basket");

        //obyek Ketua UKM
        Mahasiswa ketua = new Mahasiswa("195314119", "Rico", "Sleman,16/07/2001");
        ukm.setKetua(ketua);

        //obyek Sekretaris
        Mahasiswa sekretaris = new Mahasiswa("195314087", "Rani", "Sleman,19/05/2001");
        ukm.setSekretaris(sekretaris);

        //obyek Anggota Mahasiswa
        Mahasiswa anggota1 = new Mahasiswa("195314108", "Rudi", "Sleman,21/04/2001");
        Mahasiswa anggota2 = new Mahasiswa("195314089", "Rahma", "Sleman,20/06/2001");

        //obyek Anggota Masyarakat Sekitar
        MasyarakatSekitar anggota3 = new MasyarakatSekitar("198", "Dion", "Sleman,04/09/2001");

        Penduduk[] dataAnggota = new Penduduk[5];
        dataAnggota[0] = ketua;
        dataAnggota[1] = sekretaris;
        dataAnggota[2] = anggota1;
        dataAnggota[3] = anggota2;
        dataAnggota[4] = anggota3;

        //Tampil data
        System.out.println("UKM : " + ukm.getNamaUnit());
        System.out.println("Daftar Anggota");
        System.out.println("==============================================");
        System.out.println("No.\tNama\tNim/Nomor\tJabatan\ttTGL Lahir\tIuran");
        System.out.println("===============================================");
        int totalIuran = 0;
        for (int i = 0; i < dataAnggota.length; i++) {
            System.out.print((i + 1) + "\t" + dataAnggota[i].getNama() + "\t");
            if (i < 4) {
                dataAnggota[i] = new Mahasiswa();
                Mahasiswa mhs = (Mahasiswa) dataAnggota[i];
                System.out.print(mhs.getNim() + "\t");
            } else {
                dataAnggota[i] = new MasyarakatSekitar();
                MasyarakatSekitar umum = (MasyarakatSekitar) dataAnggota[i];
                System.out.print(umum.getNomor() + "\t");
            }
            System.out.print(dataAnggota[i].getTempatTanggalLahir()+"\t");
            if (i < 4) {
                dataAnggota[i] = new Mahasiswa();
                Mahasiswa mhs = (Mahasiswa) dataAnggota[i];
                System.out.print("Rp."+mhs.hitungIuran()+ "\t");
                totalIuran += mhs.hitungIuran();
            } else {
                dataAnggota[i] = new MasyarakatSekitar();
                MasyarakatSekitar umum = (MasyarakatSekitar) dataAnggota[i];
                System.out.print("Rp."+umum.hitungIuran() + "\t");
                totalIuran += umum.hitungIuran();
            }
        }
        System.out.println();
        System.out.println("Total iuran seluruh Anggota : Rp."+totalIuran);
    }
}
