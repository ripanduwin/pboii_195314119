/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_1;

/**
 *
 * @author asus
 */
public abstract class Penduduk {
    protected String nama;
    protected String tempatTanggalLahir;

    public Penduduk() {
    }

    public Penduduk(String nama, String tempatTanggalLahir) {
        this.nama = nama;
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }
    
    public double hitungIuran(){
        return 0;
    }

    @Override
    public String toString() {
        return "";
    }
}
